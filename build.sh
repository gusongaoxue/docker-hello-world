#!/bin/bash
#无SWARM集群管理版本

#build in jenkins 

# 你的docker仓库的地址
REG_URL=hlc1032640718

#根据时间生成版本号
TAG=$REG_URL/$JOB_NAME:`date +%y%m%d-%H-%M`

#使用maven 镜像进行编译 打包出 war 文件 （其他语言这里换成其他编译镜像）
docker run --rm --name mvn  -v /mnt/maven:/root/.m2   \
 -v /mnt/jenkins_home/workspace/$JOB_NAME:/usr/src/mvn -w /usr/src/mvn/\
 maven:3.3.3-jdk-8 mvn clean install -Dmaven.test.skip=true
 
#使用我们刚才写好的 放在项目下面的Dockerfile 文件打包 
docker build -t  $TAG  $WORKSPACE/.

#登录镜像仓库
docker login -u hlc1032640718 -p 1032640718 -e 1032640718@qq.com

#上传镜像
docker push $TAG

#删除之前的旧镜像
docker rmi $TAG


# 如果有以前运行的版本就删了
if docker ps -a | grep -i docker-hello-world; then
        docker rm -f docker-hello-world
fi

#运行docker image
docker run  -d  -p 8848:8080  --name $JOB_NAME  $TAG 



 